# gcp-costs-notify
Cloud Function to run GCP Billing notifications

## About this function 
This function queries a dataset in BigQuery that contains exported billing information for a specific billing account.
The query returns consumption on per project basis from the day before.
The results will be pushed on to Google Chat Space via a provided Webhook. 

## Prerequisites
#### 1. Export your billing data to BigQuery
Follow the guide on how to [Set up Cloud Billing data export to BigQuery](https://cloud.google.com/billing/docs/how-to/export-data-bigquery-setup) to expoert your billing information to BigQuery.

#### 2. Configure a webhook in Google Chat Space
To register the incoming webhook:
- Open Google Chat in a web browser
- Go to the space to which you want to add a webhook
- At the top, click the Chat title > Apps & Integrations > Manage webhooks
- Create a Webhook
  - Enter the name you like
  - For Avatar URL, enter https://developers.google.com/chat/images/chat-product-icon.png
- Click SAVE.
- Copy the full webhook URL to use it in the following step

#### 3. Your environment
You need [Google Cloud CLI (gcloud)](https://cloud.google.com/sdk/docs/install) installed.
To run the function locally you'd need [Go](https://go.dev/doc/install) development environment (optional).
You can use [Cloud Shell](https://cloud.google.com/shell/docs/launching-cloud-shell) for any of the above.

#### 4. Clone the repo
```
git clone https://gitlab.com/miroslawpolski/gcp-costs-notify
```


## Deploying the function

#### Running the function locally (optional)
Before deploying the function to GCP one may want to test it locally.
See [instructions](https://gitlab.com/miroslawpolski/gcp-costs-notify/blob/main/docs/running_locally.md) on how to run the function locally.

#### Deploy to GCP
Enable Google Cloud Functions API in your project:

```
gcloud services enable cloudfunctions.googleapis.com
```

Fill in the `./query/.env.yaml` file with your values of the project your BigQuery dataset is in, the BigQuery dataset details (both from step 1) as weel as your Google Chat Space webhook link (created in step 2 above):

```
PROJECT_ID: <project_name_where_BQ_dataset_lives>
DATASET: <dataset name>
BQ_TABLE_NAME: <table name>
LOCATION: <region of the dataset>
GOOGLE_CHAT_URL: <webhook URL of your Google Chat space>
EOF
```
> [!NOTE]
> If you have just enabled billing export, the table may not have yet been created. Wait a bit for the table to appear.

Deploy the function:

Set the Region you want to deploy it to and the name of the Cloud Function:
```
REGION=<region_name>
FUNCTION=<function_name>
```

Make sure you're in the `query` directory and run:
```
gcloud functions deploy $FUNCTION \
--region=$REGION \
--entry-point=query \
--runtime go116 \
--trigger-http \
--max-instances=1 \
--ingress-settings=internal-and-gclb \
--no-allow-unauthenticated \
--env-vars-file=.env.yaml
```
When asked about allowing unauthenticated invocations, answer `No`.

#### Verify the function has been deployed

```
gcloud functions list
```

Call the function to test it

```
gcloud functions call $FUNCTION --region=$REGION
```

At this point a message should appear in you Google Chat Space similar to the image below:

![alt image](images/chat_example.png)


To run the function regularly one may use Google Cloud Scheduler.

## Scheduling the function

#### Create a Service Account
First, create a Service Account and assign it a role so that it can invoke the Cloud Function.

```
PROJECT_ID=$(gcloud config list --format='value(core.project)')
SA=queryscheduler
SA_EMAIL=$SA@$PROJECT_ID.iam.gserviceaccount.com

gcloud iam service-accounts create $SA \
  --description="Scheduler Service Account to trigger Cloud Function" \
  --display-name="Scheduler Service Account for Cloud Function"

gcloud projects add-iam-policy-binding $PROJECT_ID \
  --member serviceAccount:$SA_EMAIL \
  --role roles/cloudfunctions.invoker
```

#### Enable Scheduler
Enable Scheduler service if not running in your project:
```
gcloud services enable cloudscheduler.googleapis.com
```

#### Collect the function's URI:
Get the functions URL the scheduller will call:
```
URI=$(gcloud functions describe $FUNCTION --region $REGION --format="value(httpsTrigger.url)")
```

#### Create the Scheduler Job
Next, create the Scheduler Job to trigger the function at a desired intervals.
First, set the time zone:
```
TZ="Europe/Warsaw"
```
Next, create the job:
```
gcloud scheduler jobs create http run-$FUNCTION \
  --schedule="0 8 * * *" \
  --time-zone="Europe/Warsaw" \
  --uri=$URI \
  --oidc-service-account-email=$SA_EMAIL \
  --location=$REGION
```
Verify the scheduler job is enabled:
```
gcloud scheduler jobs list --location=$REGION
```

Assign your user pe

#### Run the job now to test it.
```
gcloud scheduler jobs run run-$FUNCTION --location=$REGION
```

## Cleaning up

```
gcloud scheduler jobs delete run-$FUNCTION --quiet
gcloud iam service-accounts delete $SA_EMAIL --quiet
gcloud functions delete $FUNCTION --region=$REGION --quiet
```
